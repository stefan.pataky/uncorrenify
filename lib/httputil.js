const http = require('http');
const https = require('https');
const config = require('../config.json')

module.exports = {
    get : (h, p, cb) => {

	const options = {
	  host: h,
          path: p,
	};

	let getFn = null;

	if(config.ssl) {
	  getFn = https.get;
	} else {
	  getFn = http.get;
        }       

        console.log(options);
        
        getFn(options, function(res) {
            console.log(res.statusCode);
            let body = '';
            res.on("data", function(chunk) {
              body += chunk;
            });
            res.on('end', function() {
              cb(null, body);
            });
          }).on('error', function(e) {
            //suppress error
	    cb(e);
       	});	
	
    },
  idFromUrl : (url) => {
    let tail = url.lastIndexOf('-');
    let id = url.substr(tail+1, 9);
    return id;
  }
}

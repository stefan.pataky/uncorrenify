const express = require('express');
const router = express.Router();
const httputil = require('../lib/httputil');
const config = require('../config.json');


router.get('/', (req, res, next) => {
  let id = httputil.idFromUrl(req.baseUrl);
  httputil.get(config.source, '/ws/GetArticle.ashx?articleid=' + id , (err, data) => {
    res.render('article', { title: config.title, html : JSON.parse(data).d.html });
  });
});

module.exports = router;

const express = require('express');
const router = express.Router();
const httputil = require('../lib/httputil');
const config = require('../config.json');

const replaceAll = (str, find, replace) => {
  return str.replace(new RegExp(find, 'g'), replace);
}

router.get('/:id', (req, res, next) => {

  httputil.get(config.source, '/ws/GetArticle.ashx?articleid=' + req.params['id'], (err, data) => {

    let html =  JSON.parse(data).d.html;

    //Make links relative
    html = replaceAll(html, 'href="/', 'href="../');

    res.render('article', { title: config.title, html :html});
  });
  
});

module.exports = router;

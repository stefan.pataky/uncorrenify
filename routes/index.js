const express = require('express');
const router = express.Router();
const httputil = require('../lib/httputil');
const cheerio = require('cheerio');
const config = require('../config.json');

let rssarticles = [];
let fparticles = []

let sync = () => {
  rssarticles = [];
  fparticles = [];

  httputil.get(config.source, '/nyheter/rss/', (err, xml) => {
    getRssArticles(xml);
  });

  httputil.get(config.source, '/nyheter/', (err, html) => {
    if(err) {
      console.log(err);
      return;
    }

    let $ = cheerio.load(html);

    if($ === undefined) {
      console.log("html load was undefined");
    }
  
    $('a.blurb-link-area').each(function() {
      let text = $(this).text().trim();
      let link = $(this).attr('href');

      if(text.length === 0) return;

      if(link.indexOf('aspx') < 0) return;

      let id = httputil.idFromUrl(link);

      let article = {
        title : text,
        id : id
      }

      fparticles.push(article);

    });
  });
}

sync();

setInterval(() => {
  sync();
}, 120000);


function getRssArticles(xml)
{
  const regex = /<link>(.*?)<\/link><title>(.*?)<\/title>/g;

  while ((m = regex.exec(xml)) !== null) {
    if (m.index === regex.lastIndex) {
      regex.lastIndex++;
    }

    if(m[1].indexOf('aspx') < 0) continue;
    let id = httputil.idFromUrl(m[1]);

    let article = {
      title : m[2],
      id : id
    }

    rssarticles.push(article);
 }
}

router.get('/', function(req, res, next) {
  res.render('index', { title: config.title, rssarticles: rssarticles, fparticles:fparticles });
});

module.exports = router;

